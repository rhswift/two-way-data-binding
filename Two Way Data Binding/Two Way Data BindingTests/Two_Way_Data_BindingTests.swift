//
//  Two_Way_Data_BindingTests.swift
//  Two Way Data BindingTests
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import XCTest
@testable import TwoWayDataBinding

class Two_Way_Data_BindingTests: XCTestCase {
    
    var stringToBind: NSString!
    var valueToBindToString: NSString!
    
    override func setUp() {
        super.setUp()
        stringToBind = "stringIsBoundTo->"
        valueToBindToString = "ThisValueString"
    }
    
    override func tearDown() {
        stringToBind = nil
        valueToBindToString = nil
        super.tearDown()
    }
    
    func testInitialBinding() {
        XCTAssertEqual(stringToBind, "stringIsBoundTo->")
        XCTAssertEqual(valueToBindToString, "ThisValueString")
        
        //XCTAssert(stringToBind.bindableObject.value as AnyObject? == nil, "String doesn't have a binding present.")
        stringToBind.bindableObject ->> valueToBindToString
        XCTAssert(stringToBind.bindableObject.value as AnyObject? != nil, "String does have a binding present.")
        
    }
    
    func testTwoWayBinding() {
        let currentBinding = (stringToBind.bindableObject.value as AnyObject?) as! NSString
        XCTAssert(currentBinding == valueToBindToString, "String current binding matches the value bound.")
        
        valueToBindToString = "\(valueToBindToString)ChangedValue" as NSString!
        XCTAssert(currentBinding == valueToBindToString, "String current binding STILL matches the value bound.")
        
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
