//
//  BondableObject.swift
//  Two Way Data Binding
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation

public class BondableObject<T> {
    
    typealias Listener = ((T) -> Void)
    
    var value: T {
        didSet {
            for element in bonds {
                element.bond?.listener(value)
            }
        }
    }
    
    var bonds: [BondBox<T>] = []
    
    init(_ v: T) {
        value = v
    }
}

public class BondObject<T> {
    public var value: T?
    
    init() {}
}
