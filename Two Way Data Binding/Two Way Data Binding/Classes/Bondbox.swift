//
//  Bondbox.swift
//  Two Way Data Binding
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation

public class BondBox<T> {
    
    weak var bond: Bind<T>?
    
    init(_ bond: Bind<T>) {
        self.bond = bond
    }
}
