//
//  Bind.swift
//  Two Way Data Binding
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation

public class Bind<T> {
    
    typealias Listener = ((T) -> Void)
    
    var listener: Listener
    
    init(_ listener: @escaping Listener) {
        self.listener = listener
    }
    
    func bind(object: BondableObject<T>) {
        object.bonds.append(BondBox(self))
    }
}
