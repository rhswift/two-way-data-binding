//
//  TWDB_Extensions.swift
//  TwoWayDataBinding
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation

private var handle: UInt8 = 0;
private var bondHandle: UInt8 = 1;
private var bondableHandle: UInt8 = 2;

extension NSObject {
    
    fileprivate func associatedObject<ValueType: AnyObject>(base: AnyObject, key: UnsafePointer<UInt8>, initialiser:() -> ValueType)
        -> ValueType {
            if let associated = objc_getAssociatedObject(base, key)
                as? ValueType { return associated }
            let associated = initialiser()
            objc_setAssociatedObject(base, key, associated,
                                     .OBJC_ASSOCIATION_RETAIN)
            return associated
    }
    
    fileprivate func associateObject<ValueType: AnyObject>(base: AnyObject, key: UnsafePointer<UInt8>, value: ValueType) {
        objc_setAssociatedObject(base, key, value,.OBJC_ASSOCIATION_RETAIN)
    }
    
}

extension NSObject {
    
    private var bond: BondObject<Any> {
        get {
            return associatedObject(base: self, key: &bondHandle, initialiser: {
                return BondObject<Any>()
            })
        }
        set {
           associateObject(base: self, key: &bondHandle, value: newValue)
        }
    }
    
    fileprivate var binding: Bind<Any> {
        if let b: AnyObject = objc_getAssociatedObject(self, &handle) as AnyObject? {
            return b as! Bind<Any>
        } else {
            let bind = Bind<Any> {
                v in
                self.bond.value = v
            }
            associateObject(base: self, key: &handle, value: bind)
            return bind
        }
    }
    
    public var bindableObject: BondableObject<Any> {
        if let b: AnyObject = objc_getAssociatedObject(self, &bondableHandle) as AnyObject? {
            return b as! BondableObject<Any>
        } else {
            let bind = BondableObject<Any> {
                v in
                self.bond.value = v
            }
            associateObject(base: self, key: &bondableHandle, value: bind)
            return bind
        }
    }
    
}

extension NSObject: Bindable {
    
    public var designatedBindng: Bind<Any> {
        return binding
    }
    
    
    
}
