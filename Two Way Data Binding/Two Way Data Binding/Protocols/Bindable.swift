//
//  Bindable.swift
//  Two Way Data Binding
//
//  Created by Romaine Hinds on 9/28/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation

precedencegroup BindingPrecedence {
    associativity: left
    higherThan: LogicalConjunctionPrecedence
}

infix operator ->> : BindingPrecedence

public func ->> <T>(left: BondableObject<T>, right: Bind<T>) {
    right.bind(object: left)
}

public func ->> <T, U: Bindable>(left: BondableObject<T>, right: U) where U.BindType == T {
    left ->> right.designatedBindng
}


public protocol Bindable {
    
    associatedtype BindType
    var designatedBindng: Bind<BindType> { get }
    
}
